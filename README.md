# script-xanmod-installer-unofficial

- This script was created by manoel-linux for my favorite custom kernel. It is not an official script from XanMod, but rather a personal creation for specific use. XanMod is a customized Linux kernel primarily maintained by Alexandre Frade. More information about XanMod can be found on GitHub at https://github.com/xanmod/linux and on GitLab at https://gitlab.com/xanmod/linux                             

- Please note that the use of this script is the responsibility of the user and is not associated with the official XanMod project. 
XanMod is licensed under the CC BY-NC-SA 4.0 license. Please read the license terms at https://creativecommons.org/licenses/by-nc-sa/4.0/ to understand the rights and restrictions associated with the use of XanMod. For more information, please visit the official 
XanMod website at https://xanmod.org

- script-xanmod-unofficial-installer: sep 2023

- build-latest: 0.0.3

- Support for the distro: Ubuntu/Debian

- Use at your own risk

- You have to execute it as a superuser or with sudo

## Installation

- To install xanmod, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/script-xanmod-installer-unofficial.git

# 2. To install the script-xanmod-installer-unofficial script, follow these steps

- chmod a+x `script-xanmod-installer-unofficial.sh`

- sudo `./script-xanmod-installer-unofficial.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The script-xanmod-installer-unofficial project is currently in development. The latest stable version is 0.0.3. We aim to provide regular updates and add more features in the future.

# License

- script-xanmod-installer-unofficial is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of script-xanmod-installer-unofficial.
